package controller

import (
	"goland-gin-poc/entity"
	"goland-gin-poc/services"
	"goland-gin-poc/validators"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
)

type VideoController interface {
	Save(ctx *gin.Context) (entity.Video, error)
	All() []entity.Video
}

type videoController struct {
	service services.VideoService
}

var validate *validator.Validate

func New(service services.VideoService) VideoController {
	validate = validator.New()
	validate.RegisterValidation("notNumber", validators.ValidateNotNumberName)
	return &videoController{
		service: service,
	}
}

func (controller *videoController) Save(ctx *gin.Context) (entity.Video, error) {
	var video entity.Video
	err := ctx.ShouldBindJSON(&video)
	if err != nil {
		return video, err
	}
	err = validate.Struct(video)
	if err != nil {
		return video, err
	}
	controller.service.Save(video)
	return video, nil
}

func (controller *videoController) All() []entity.Video {
	return controller.service.All()
}
