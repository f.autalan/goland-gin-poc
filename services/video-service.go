package services

import "goland-gin-poc/entity"

type VideoService interface {
	Save(entity.Video) entity.Video
	All() []entity.Video
}

type videoService struct {
	videos []entity.Video
}

func New() VideoService {
	return &videoService{}
}

func (service *videoService) Save(video entity.Video) entity.Video {
	service.videos = append(service.videos, video)
	return video
}

func (service *videoService) All() []entity.Video {
	return service.videos
}
