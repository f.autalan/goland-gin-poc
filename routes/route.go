package routes

import (
	"goland-gin-poc/controller"
	"goland-gin-poc/middleware"
	"goland-gin-poc/services"
	"net/http"

	"github.com/gin-gonic/gin"
)

var (
	videService     services.VideoService      = services.New()
	videoController controller.VideoController = controller.New(videService)
)

func Route(r *gin.Engine) {
	r.Use(middleware.BasicAuth())
	r.GET("/videos", func(ctx *gin.Context) {
		ctx.JSON(http.StatusOK, videoController.All())
	})
	r.POST("/videos", func(ctx *gin.Context) {
		response, err := videoController.Save(ctx)
		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
		} else {
			ctx.JSON(http.StatusCreated, response)
		}
	})
}
