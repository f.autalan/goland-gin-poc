package main

import (
	"goland-gin-poc/logs"
	"goland-gin-poc/middleware"
	"goland-gin-poc/routes"

	"github.com/gin-gonic/gin"
)

func main() {
	logs.SetupLogOutput()
	server := gin.Default()
	server.Use(middleware.BasicAuth())
	routes.Route(server)
	server.Run(":8080")
}
