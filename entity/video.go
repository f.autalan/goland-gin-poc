package entity

type Video struct {
	Title       string `json:"title" binding:"required,min=2,max=10"`
	Description string `json:"description" binding:"required,min=10"`
	URL         string `json:"url" binding:"required,url"`
	Author      Person `json:"author" binding:"required"`
}
