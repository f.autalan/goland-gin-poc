package entity

type Person struct {
	FirstName string `json:"firstName" binding:"required" validate:"notNumber"`
	LastName  string `json:"lastName" binding:"required" validate:"notNumber"`
	Age       int8   `json:"age" binding:"required,gte=14,lte=110"`
	Email     string `json:"email" binding:"required,email"`
}
