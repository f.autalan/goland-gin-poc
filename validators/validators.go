package validators

import (
	"fmt"
	"unicode"

	"github.com/go-playground/validator/v10"
)

func ValidateNotNumberName(field validator.FieldLevel) bool {
	name := field.Field().String()
	fmt.Println(name)
	for i := 0; i < len(name); i++ {
		if unicode.IsNumber(rune(name[i])) {
			return false
		}
	}
	return true
}
